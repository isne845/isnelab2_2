#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if(tail==0)
	{
		tail = head;
	}
	else
	{	
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	Node<T>* t = new Node<T>(el);
	tail->next = t;
	t->prev = tail;
	tail = t;
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node<T>* p = tail;
	T store = tail->data; //store value before delete
	tail = tail->prev; //point to another way
	delete p;
	tail->next = 0;
	return store;
}
bool List::search(char el)
{
	Node<T>* f = head;

	while (f != 0) {
		if (f->data == el) {
			return true;
		}
		else {
			f = f->next;
		}
	}

	return false;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
